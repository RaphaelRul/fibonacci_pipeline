"""
Your module doc goes here.

Add a couple of lines here.
"""
# -*- coding: utf-8 -*-
def fibonacci(n):
    """
    Calculate numbers in the fibonacci sequence.

    Parameters
    ----------
    n : int
        The position of the number un the sequence?

    Returns
    -------
    result : int
             The n'th number in the fibonacci sequence.
    """
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    result = a
    return result
