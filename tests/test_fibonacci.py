import sys
module_path = "../" #or "../..
if module_path not in sys.path:
   sys.path.append(module_path)

from fibonacci import fibonacci

def test_fibonacci_first_sequence():
    assert fibonacci(1) == 1

def test_fibonacci_seconde_sequence():
    assert fibonacci(2) == 1

def test_fibonacci_negative_number():
    pass
